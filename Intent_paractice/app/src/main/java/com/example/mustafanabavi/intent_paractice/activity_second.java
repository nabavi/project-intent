package com.example.mustafanabavi.intent_paractice;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Mustafa Nabavi on 4/8/2017.
 */

public class activity_second extends Activity {

    TextView name_txt, age_txt , descrip_txt;
    Intent intent ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.second_layout);

        name_txt = (TextView)findViewById(R.id.text_name);
        age_txt = (TextView)findViewById(R.id.text_age);
        descrip_txt = (TextView)findViewById(R.id.text_desc);



        intent = getIntent();
        name_txt.setText(intent.getStringExtra("1"));

        age_txt.setText(intent.getStringExtra("2"));
        descrip_txt.setText(intent.getStringExtra("3"));



    }
}
