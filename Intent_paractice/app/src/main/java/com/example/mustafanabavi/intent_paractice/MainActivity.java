package com.example.mustafanabavi.intent_paractice;

import android.app.Application;
import android.app.Notification;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    EditText name_text , age_text , description_text;
    Button btn_add, btn_remove;
    Intent intent ;
    String name ,descrip ,age ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        btn_add = (Button)findViewById(R.id.btn_add);
        btn_remove = (Button)findViewById(R.id.btn_remove);

        name_text = (EditText)findViewById(R.id.editText_name);
        age_text = (EditText)findViewById(R.id.editText_age);
        description_text = (EditText)findViewById(R.id.editText_desc);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                 name =  name_text.getText().toString();
                descrip = description_text.getText().toString();
                age = age_text.getText().toString();



                intent = new Intent(MainActivity.this,activity_second.class);
                intent.putExtra("1",name);
                intent.putExtra("2",age);
                intent.putExtra("3",descrip);

                startActivity(intent);



            }
        });



    }
}
